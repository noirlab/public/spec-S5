.. title: Index
.. slug: index
.. date: 2024-11-07 00:00:00
.. tags:
.. category:
.. link:
.. description:
.. type: text
.. author: Spec-S5
.. has_math: true
.. hidetitle: true

.. container:: col-md-2

   .. image:: ../files/SpecS5logo.001.jpeg
       :height: 100
       :width: 230

.. container:: col-md-2

   .. class:: jumbotron-special

   .. raw:: html

      <h3><a href="science">Ciencia</h3>

.. container:: col-md-2

   .. class:: jumbotron-special

   .. raw:: html

      <h3><a href="instrument">Instrumento</h3>
	      
.. container:: col-md-2

   .. class:: jumbotron-special

   .. raw:: html

      <h3><a href="meetings">Reuniones</h3>
	      
.. container:: col-md-2

   .. class:: jumbotron-special

   .. raw:: html

      <h3><a href="documents">Documentos</h3>
	      
.. container:: col-md-2

   .. class:: jumbotron-special

   .. raw:: html

      <h3><a href="community">Comunidad</h3>


.. container:: col-md-6 col-right

   .. image:: ../files/S5_blanco.001.png
       :height: 400
       :width: 790

.. container:: col-md-6 col-left

   .. class:: jumbotron

   .. raw:: html

      <h3>Spec-S5</h3>
      <p style="font-size:17px"> Con su primera luz en la década de 2030, Spec-S5 será el principal instrumento para investigar ambas épocas de expansión acelerada. Al mapear 10 veces más modos lineales que DESI + Rubin + CMB-S4, aumentará drásticamente nuestro conocimiento de la física del universo temprano al mapear la huella de las características primordiales en un enorme volumen del universo distante. Siga los enlaces anteriores para obtener más información sobre nuestra ciencia clave, diseño de instrumentos, reuniones, documentos y comunidad.</p>
      
   .. class:: jumbotron

   .. raw:: html

      <h3>Commitment to equity, diversity, and inclusion</h3>
      <p style="font-size:17px">El Proyecto Spec-S5 respeta a todas las personas y espera un entorno positivo, solidario e inclusivo entre sus miembros y partes interesadas. Haga clic aquí para leer las declaraciones de Spec-S5 sobre Equidad, Diversidad e Inclusión.</p>




