.. title: Meetings
.. slug: meetings
.. date: 2024-06-11 00:00:00
.. tags: Contact

.. class:: pull-right well

.. contents::

We have had many productive meetings and workshops to date!
If you have ideas for future workshops please contact anyone on our leadership team.
   
Past Meetings
---------------
`Oct 2023: Feasability of using NOIRLab platforms for Spec-S5 instruments`_
    * Hosted at Noirlab Headquartes, Tuscon, AZ
`Feb 2024: Spec-S5 Instrumentation`_ 
    * Hosted by the Kavli Institute for Cosmological Physics (KICP)
`May 2024: Fundamental Phsyics from Future Spectroscopic Surveys`_
    * Hosted at LBNL, Berkeley, CA



Future Meetings
---------------
* Positioner focused workshop (May 1&2 2025, LBNL, ~30 people)
    * Explore the landscape of fiber positioning robots for Spec-S5. Many designs of fiber positioning robots that meet requirements for Spec-S5 are currently being designed and developed and are at various stages of maturity. We will look at each of these designs with a particular focus on positioning performance, interfaces with the fiber system, manufacturability, and plan for operations. The goal of the workshop is to understand the advantages and limitations for each design to be able to converge on a baseline in the future"
    * Registration link will be added
    * Contact: Claire Poppett (clpoppett@lbl.gov) 
* Instrument workshop 2 (2025)
    * In planning stages
* Science (non core to focus on broader science (2024)
    * Contact Arjun Dey <arjun.dey@noirlab.edu>
    * Contact Alex Drlica-Wagner <kadrlica@uchicago.edu>



.. _`Spec-S5 Code of Conduct`: https://docs.google.com/document/d/13xAm2P2jont-7gYBCaH2ra73EmKX2ssZ/edit?usp=drive_link&ouid=103995188625312094513&rtpof=true&sd=true
.. _`Oct 2023: Feasability of using NOIRLab platforms for Spec-S5 instruments`: https://desi.lbl.gov/trac/wiki/PublicPages/Meetings/Feasibility%20of%20using%20noirlab%20platforms%20for%20Spec-S5%20instruments
.. _`Feb 2024: Spec-S5 Instrumentation`: https://kicp-workshops.uchicago.edu/2024-SpecS5/overview.php
.. _`May 2024: Fundamental Phsyics from Future Spectroscopic Surveys`: https://indico.physics.lbl.gov/event/2769/

