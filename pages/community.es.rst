.. title: Community
.. slug: community
.. date: 2024-03-10 00:00:00
.. tags: Contact

.. class:: pull-right well

.. contents::

We invite you to participate in Spec-S5 development!
The mailing list is spec5-general@desi.lbl.gov
Please feel free to contact anyone on our leadership team.

`Please join our slack channel by clicking on this link`_

.. _`Please join our slack channel by clicking on this link`: https://join.slack.com/t/spec-s5/shared_invite/zt-2yaow19ts-cPtzRvVAy2hCJU9j19V1YA

Leadership Team
---------------
David Schlegel <DJSchlegel@lbl.gov>, Claire Poppett <clpoppett@lbl.gov>, Klaus Honscheid <kh@physics.osu.edu>, Arjun Dey <arjun.dey@noirlab.edu>, Parker Fagrelius <parker.fagrelius@noirlab.edu>, Michael Levi <melevi@lbl.gov>


Code of Conduct
---------------
`Spec-S5 Code of Conduct`_

.. _`Spec-S5 Code of Conduct`: https://docs.google.com/document/d/13xAm2P2jont-7gYBCaH2ra73EmKX2ssZ/edit?usp=drive_link&ouid=103995188625312094513&rtpof=true&sd=true

Partnering Institutions
-----------------------

.. image:: /files/UCBerkeley.png
   :height: 70px
   :alt: UC Berkeley Logo
.. image:: /files/Berkeley_Lab_Logo_Small.png
   :height: 80px
   :alt: Berkeley Lab Logo
.. image:: /files/doeOOS.jpg
   :height: 40px
   :alt: Department of Energy Logo
.. image:: /files/OhioState.png
   :height: 65px
   :width: 80px
   :alt: Ohio State Logo
.. image:: /files/nersc-logo.png
   :height: 40px
   :alt: NERSC Logo
.. image:: /files/SSL.png
   :height: 70px
   :alt: SSL Logo
.. image:: /files/nsf-noirlab.jpg
   :height: 80px
   :alt: NOIRLab Logo
.. image:: /files/AURAlogo.jpg
   :height: 40px
   :alt: AURA Logo
